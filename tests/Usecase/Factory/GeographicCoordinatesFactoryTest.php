<?php


namespace App\Tests\Usecase\Factory;


use App\Usecase\Factory\GeographicCoordinatesFactory;
use PHPUnit\Framework\TestCase;

class GeographicCoordinatesFactoryTest extends TestCase
{
    public function testShouldReturnGeoCoordinatesValueObjectWithGoodValues()
    {
        // Given
        $positions = [];
        $positions['latitude'] = 60;
        $positions['longitude'] = 120;
        $factory = new GeographicCoordinatesFactory();
        // When
        $valueObject = $factory->build($positions);
        // Then
        $this->assertEquals(60, $valueObject->getLatitude());
        $this->assertEquals(120, $valueObject->getLongitude());
    }

}
