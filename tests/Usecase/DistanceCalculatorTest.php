<?php


namespace App\Tests\Usecase;


use App\Domain\ValueObject\GeographicCoordinates;
use App\Usecase\DistanceCalculator;
use PHPUnit\Framework\TestCase;

class DistanceCalculatorTest extends TestCase
{
    public function testShouldReturnCalculatedDistanceValueBetweenTwoPoints()
    {
        // Given
        $point = new GeographicCoordinates(50, 120);
        $target = new GeographicCoordinates(55, 130);
        // When
        $calculator = new DistanceCalculator();
        $distance = $calculator->calculate($point, $target);
        // Then
        $this->assertEquals(874535.850872291, $distance);
    }

}
