<?php


namespace App\Tests\Adapter;


use App\Domain\Exception\NoPositionSectionInInformationArrayException;
use App\Domain\ValueObject\GeographicCoordinates;
use App\Adapter\ISSCoordinatesProvider;
use PHPUnit\Framework\TestCase;

class ISSCoordinatesProviderTest extends TestCase
{
    private ISSCoordinatesProvider $provider;
    protected function setUp(): void
    {
        $this->provider = new ISSCoordinatesProvider();
    }

    public function testShouldReturnExceptionIfArrayDontHavePositionsSection()
    {
        // Expect
        $this->expectException(NoPositionSectionInInformationArrayException::class);
        // Given
        $array = [];
        // When
        $this->provider->getCoordinatesFromInformationArray($array);
    }
    
    public function testShouldReturnLatitudeAndLongitudeFromArray()
    {
        // Given
        $array = [
            "timestamp" => 1619967676,
            "message" => "success",
            "iss_position" => [
                "latitude" => "32.4007",
                "longitude" => "-88.1600",
            ]
        ];
        // When
        $geoCoordinates =$this->provider->getCoordinatesFromInformationArray($array);
        // Then
        $this->assertInstanceOf(GeographicCoordinates::class, $geoCoordinates);
        $this->assertEquals(32.4007, $geoCoordinates->getLatitude());
        $this->assertEquals(-88.1600, $geoCoordinates->getLongitude());
    }
    
}
