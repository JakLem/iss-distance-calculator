<?php


namespace App\Tests\Domain\ValueObject;


use App\Domain\Exception\GeographicCoordinatesValuesNotValidException;
use App\Domain\ValueObject\GeographicCoordinates;
use PHPUnit\Framework\TestCase;

class GeographicCoordinatesTest extends TestCase
{
    public function testShouldThrowExceptionIfLatitudeTooBig()
    {
        // Expect
        $this->expectException(GeographicCoordinatesValuesNotValidException::class);
        // Given
        new GeographicCoordinates(91, 180);
    }

    public function testShouldThrowExceptionIfLatitudeTooSmall()
    {
        // Expect
        $this->expectException(GeographicCoordinatesValuesNotValidException::class);
        // Given
        new GeographicCoordinates(-91, 180);
    }

    public function testShouldThrowExceptionIfLongitudeTooBig()
    {
        // Expect
        $this->expectException(GeographicCoordinatesValuesNotValidException::class);
        // Given
        new GeographicCoordinates(181, 90);
    }

    public function testShouldThrowExceptionIfLongitudeTooSmall()
    {
        // Expect
        $this->expectException(GeographicCoordinatesValuesNotValidException::class);
        // Given
        new GeographicCoordinates(-181, 90);
    }

}
