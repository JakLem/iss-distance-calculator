<?php


namespace App\Tests\Domain\Entity;


use App\Domain\Entity\DistanceBetweenPointAndTarget;
use App\Domain\ValueObject\GeographicCoordinates;
use PHPUnit\Framework\TestCase;

class DistanceBetweenPointAndTargetTest extends TestCase
{
    public function testShouldCalculateDistanceBetweenTwoPointsOnEarth()
    {
        // Given
        $point1 = new GeographicCoordinates(52.4004458,16.7615836);
        $point2 = new GeographicCoordinates( 53.01375,18.59814);
        $ISSDistanceFromTarget = new DistanceBetweenPointAndTarget($point1, $point2);
        // When
        $distance = $ISSDistanceFromTarget->getDistance(); //Haversine formula
        // Then
        $this->assertEquals(141275.1782378118, $distance);
    }
    
}
