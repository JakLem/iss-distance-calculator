<?php


namespace App\Usecase\Factory;


use App\Domain\ValueObject\GeographicCoordinates;

class GeographicCoordinatesFactory
{
    /**
     * @param array $positions
     * @return GeographicCoordinates
     */
    public function build(array $positions) : GeographicCoordinates
    {
        return new GeographicCoordinates($positions['latitude'], $positions['longitude']);
    }
}
