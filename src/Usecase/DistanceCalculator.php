<?php


namespace App\Usecase;

use App\Domain\Entity\DistanceBetweenPointAndTarget;
use App\Domain\ValueObject\GeographicCoordinates;

class DistanceCalculator
{
    /**
     * @param GeographicCoordinates $coordinates
     * @param GeographicCoordinates $targetCoordinates
     * @return float
     */
    public function calculate(
        GeographicCoordinates $coordinates,
        GeographicCoordinates $targetCoordinates
    ): float
    {
        $distanceFromTarget = new DistanceBetweenPointAndTarget($coordinates, $targetCoordinates);

        return $distanceFromTarget->getDistance();
    }
}
