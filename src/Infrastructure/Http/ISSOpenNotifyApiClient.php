<?php


namespace App\Infrastructure\Http;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ISSOpenNotifyApiClient implements ISSDataApiClientInterface
{
    public function __construct(
        private HttpClientInterface $httpClient,
        private ParameterBagInterface $parameterBag
    )
    {}

    /**
     * @return ResponseInterface
     * @throws TransportExceptionInterface
     */
    public function getISSInformation(): ResponseInterface
    {
        return $this->httpClient->request("GET", $this->parameterBag->get("ISSApiOpenNotifyUrl"));
    }
}
