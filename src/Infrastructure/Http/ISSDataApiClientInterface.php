<?php


namespace App\Infrastructure\Http;


use Symfony\Contracts\HttpClient\ResponseInterface;

interface ISSDataApiClientInterface
{
    public function getISSInformation() : ResponseInterface;
}
