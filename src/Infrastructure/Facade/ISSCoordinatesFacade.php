<?php


namespace App\Infrastructure\Facade;

use App\Adapter\ISSCoordinatesProvider;
use App\Domain\Exception\NoPositionSectionInInformationArrayException;
use App\Domain\Exception\NoCoordinatesSectionInResponseException;
use App\Domain\ValueObject\GeographicCoordinates;
use App\Infrastructure\Http\ISSOpenNotifyApiClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * This class is getting ISS coordinates from api client.
 * Class ISSCoordinatesFacade
 * @package App\Infrastructure\Facade
 */
class ISSCoordinatesFacade
{
    /**
     * @var ISSCoordinatesProvider
     */
    private ISSCoordinatesProvider $provider;
    /**
     * @var ISSOpenNotifyApiClient
     */
    private ISSOpenNotifyApiClient $issApiClient;

    public function __construct(
        private HttpClientInterface $httpClient,
        private ParameterBagInterface $parameterBag
    )
    {
        $this->provider = new ISSCoordinatesProvider();

        $this->issApiClient = new ISSOpenNotifyApiClient(
            $this->httpClient,
            $this->parameterBag
        );
    }

    /**
     * @return GeographicCoordinates
     * @throws NoCoordinatesSectionInResponseException
     * @throws NoPositionSectionInInformationArrayException
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function getCoordinates() : GeographicCoordinates
    {
        try {
            $issApiResponse = $this->issApiClient->getISSInformation();
            $issInformationArray = $issApiResponse->toArray();

            return $this->provider->getCoordinatesFromInformationArray($issInformationArray);
        } catch (TransportExceptionInterface $e) {
            throw new NoPositionSectionInInformationArrayException();
        } catch (\Exception $e) {
            throw new NoCoordinatesSectionInResponseException();
        }
    }
}
