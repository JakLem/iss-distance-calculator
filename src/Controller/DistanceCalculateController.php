<?php


namespace App\Controller;

use App\Adapter\ISSCoordinatesProvider;
use App\Domain\Exception\NoCoordinatesSectionInResponseException;
use App\Domain\Exception\NoPositionSectionInInformationArrayException;
use App\Domain\ValueObject\GeographicCoordinates;
use App\Infrastructure\Facade\ISSCoordinatesFacade;
use App\Infrastructure\Http\ISSOpenNotifyApiClient;
use App\Usecase\DistanceCalculator;
use App\Infrastructure\Facade\DistanceFromISSFacade;
use Swagger\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Contrller is on infrastructure layer.
 * Class DistanceCalculateController
 * @package App\Controller
 */
class DistanceCalculateController extends AbstractController
{
    /**
     * @param HttpClientInterface $httpClient
     * @param ParameterBagInterface $parameterBag
     * @return JsonResponse
     * @SWG\Response(
     *     response=200,
     *     description="Returns distance between ISS and another target",
     * )
     * @SWG\Tag(name="ISS Data")
     */
    public function calculateDistance(
        HttpClientInterface $httpClient,
        ParameterBagInterface $parameterBag
    ): JsonResponse
    {
        $poznan = new GeographicCoordinates(52.4004458,16.7615836);
        $facade = new ISSCoordinatesFacade($httpClient, $parameterBag);
        $issCoordinates = $facade->getCoordinates();

        $distanceCalculator = new DistanceCalculator();
        $distance = $distanceCalculator->calculate($issCoordinates, $poznan);

        return new JsonResponse($distance, 200);
    }
}
