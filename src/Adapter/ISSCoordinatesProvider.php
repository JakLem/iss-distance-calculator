<?php


namespace App\Adapter;

use App\Domain\Exception\NoPositionSectionInInformationArrayException;
use App\Domain\ValueObject\GeographicCoordinates;
use App\Usecase\Factory\GeographicCoordinatesFactory;

class ISSCoordinatesProvider
{
    private GeographicCoordinatesFactory $coordinatesFactory;

    /**
     * ISSCoordinatesProvider constructor.
     */
    public function __construct()
    {
        $this->coordinatesFactory = new GeographicCoordinatesFactory();
    }

    /**
     * @param array $informationArray
     * @return GeographicCoordinates
     * @throws \Exception
     */
    public function getCoordinatesFromInformationArray(array $informationArray): GeographicCoordinates
    {
        if (!array_key_exists("iss_position", $informationArray)) {
            throw new NoPositionSectionInInformationArrayException();
        }
        $positions = $informationArray["iss_position"];
        return $this->coordinatesFactory->build($positions);
    }
}
