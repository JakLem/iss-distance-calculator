<?php


namespace App\Domain\Exception;


use Throwable;

class GeographicCoordinatesValuesNotValidException extends \Exception
{
    /**
     * GeographicLatitudeNotValidException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "geographic latitude or longitude value is too big or too low", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
