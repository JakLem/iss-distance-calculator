<?php


namespace App\Domain\Exception;


use Throwable;

class NoPositionSectionInInformationArrayException extends \Exception
{
    /**
     * GeographicLatitudeNotValidException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "no position section in iss data array", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
