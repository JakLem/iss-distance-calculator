<?php


namespace App\Domain\Exception;


use Throwable;

class NoCoordinatesSectionInResponseException extends \Exception
{
    /**
     * GeographicLatitudeNotValidException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "cannot find coordinates section in iss api response", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
