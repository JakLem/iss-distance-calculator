<?php


namespace App\Domain\Exception;


use Throwable;

class CannotGetISSInformationFromApiException extends \Exception
{
    /**
     * GeographicLatitudeNotValidException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "cannot get iss information from external api", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
