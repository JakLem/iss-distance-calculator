<?php


namespace App\Domain\Entity;

use App\Domain\ValueObject\GeographicCoordinates;

class DistanceBetweenPointAndTarget
{
    private float $distance;

    public function __construct(
        private GeographicCoordinates $coordinates,
        private GeographicCoordinates $target,
    )
    {
        $this->calculate();
    }

    /**
     * Haversine Formula
     * Calculating distance between two points from class property.
     * Result unit is meters.
     */
    private function calculate(): void
    {
        $earthRadius = 6371000;

        // convert from degrees to radians
        $latFrom = deg2rad($this->coordinates->getLatitude());
        $lonFrom = deg2rad($this->coordinates->getLongitude());
        $latTo = deg2rad($this->target->getLatitude());
        $lonTo = deg2rad($this->target->getLongitude());

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);

        $this->distance =  $angle * $earthRadius;
    }

    public function getDistance() : float
    {
        return $this->distance;
    }
}
