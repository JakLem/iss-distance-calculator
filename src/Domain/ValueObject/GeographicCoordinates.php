<?php


namespace App\Domain\ValueObject;


use App\Domain\Exception\GeographicCoordinatesValuesNotValidException;

class GeographicCoordinates
{
    public function __construct(
        private float $latitude,
        private float $longitude,
    )
    {
        $this->valid();
    }

    private function valid(): void
    {
        if (
            $this->latitude > 90 || $this->latitude < -90 ||
            $this->longitude > 180 || $this->longitude < -180
        ) {
            throw new GeographicCoordinatesValuesNotValidException();
        }
    }

    public function getLatitude(): float
    {
        return $this->latitude;
    }

    public function getLongitude(): float
    {
        return $this->longitude;
    }
}
